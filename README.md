Instructions for run PHP script Ubuntu:
Install git : 
sudo apt update
sudo apt install git

Install composer : 
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '795f976fe0ebd8b75f26a6dd68f78fd3453ce79f32ecb33e7fd087d39bfeb978342fb73ac986cd4f54edd0dc902601dc') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"

Install mysql:
sudo apt install mysql-server

sudo mysql_secure_installation // For setup root user
sudo mysql -u root -p 'password_here' 
GRANT ALL PRIVILEGES ON * . * TO 'catalyst'@'localhost' IDENTIFIED BY 'UOiWGpbqM4TcHUAQ'; // Create DB user for script
FLUSH PRIVILEGES;
quit

Install php:
sudo apt install php libapache2-mod-php php-mysql

Run project:
git clone https://gitlab.com/ganbayar/catalyst.git
composer install
cd catalyst
run user_upload.php

run logic_test.php // Can see logic test result