<?php

// Database connect function
function db_connect($host, $user, $password, $db_name)
{
    // Connect to MySQL
    $link = mysqli_connect($host, $user, $password);
    if (!$link) {
        die('Could not connect database: ' . $link->error);
    }

    // Make db the current database
    $db_selected = mysqli_select_db($link, $db_name);

    if (!$db_selected) {
        // If we couldn't, then it either doesn't exist, or we can't see it.
        $sql = 'CREATE DATABASE ' . $db_name . ' CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci';

        if (mysqli_query($link, $sql)) {
            echo "Database " . $db_name . " created successfully \n";
        } else {
            echo "Error creating database: " . $link->error . "\n";
        }
    }

    return $link;
}

//Create table function
function create_table($host, $user, $password, $db_name)
{
    $link = db_connect($host, $user, $password, $db_name);

    // Make table the current database
    if (mysqli_query($link, 'DROP TABLE IF EXISTS user_table')) {
        $sql = 'CREATE TABLE `user_table` ( 
            `id` INT(11) NOT NULL AUTO_INCREMENT , 
            `name` VARCHAR(255) NOT NULL , 
            `surname` VARCHAR(255) NOT NULL , 
            `email` VARCHAR(100) NOT NULL , 
            PRIMARY KEY (`id`), UNIQUE (`email`)) 
            ENGINE = InnoDB';
        if (mysqli_query($link, $sql)) {
            echo "user_table created successfully \n";
        } else {
            echo "Error creating table: " . $link->error . "\n";
        }
    } else {
        echo "Error creating table: " . $link->error . "\n";
    }


    return $link;
}

//Read csv file function
function read_csv($file)
{
    $row = 1;
    $data = [];
    // Read csv file
    if (($handle = fopen($file, "r")) !== FALSE) {
        while (($csvData = fgetcsv($handle)) !== FALSE) {
            // Skip first row
            if ($row !== 1) {
                // Catch columns
                if (filter_var($csvData[2], FILTER_VALIDATE_EMAIL)) {
                    $data[] = [
                        'name' => ucfirst(mb_strtolower($csvData[0])),
                        'surname' => ucfirst($csvData[1]),
                        'email' => $csvData[2]
                    ];
                } else {
                    echo "invalid email: " . $csvData[2] . "\n";
                }

            }
            ++$row;
        }
        fclose($handle);
    }

    return $data;

}

//Insert data to database function
function insert_data($file)
{
    $link = create_table('localhost', 'catalyst', 'UOiWGpbqM4TcHUAQ', 'catalyst');
    $datas = read_csv($file);
    $i=1;
    foreach ($datas as $data) {
        //Insert query
        $insert = 'INSERT INTO `user_table` (
                        `id`, 
                        `name`, 
                        `surname`, 
                        `email`
                        ) VALUES (
                        NULL, 
                        "' . $data['name'] . '", 
                        "' . $data['surname'] . '", 
                        "' . $data['email'] . '"
                        )';
        //Executing query
        if (mysqli_query($link, $insert)) {
            echo "user successfully created successfully \n";
            $i++;
        } else {
            echo "Error inserting user: " . $link->error . "\n";
        }
    }

    echo 'Successfully added: '.$i.' users';
}

insert_data('users.csv');

//EOF